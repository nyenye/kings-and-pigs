extends AnimatedSprite

class_name Animator

signal on_animation_finished;
signal on_animation_started;

var _animation_queue : Array = Array();

var _last_finished_animation : String = "";

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("animation_finished", self, "_on_animation_finished");

func start_animation(anim : String) -> void:
	
	_animation_queue.clear();
	
	emit_signal("on_animation_started", anim);
	
	play(anim);

func queue_animation(anim: String) -> void:
	
	if (animation == "Idle"): 
		start_animation(anim);
		start_animation(anim);
	
	else :
		_animation_queue.push_back(anim);

func get_animation_duration(animation : String) -> float:
	
	var frames_count : int = frames.get_frame_count(animation);
	
	var fps : int = frames.get_animation_speed(animation);
	
	var duration : float = frames_count * (1.0 / fps);
	
	return duration

func _on_animation_finished():
	
	if (frames.get_animation_loop(animation)): return;
	
	_last_finished_animation = animation;
	
	emit_signal("on_animation_finished", animation);
	
	if (_animation_queue.empty()): return;
	
	var animation = _animation_queue.pop_front();
	
	emit_signal("on_animation_started", animation);
	
	play(animation);
