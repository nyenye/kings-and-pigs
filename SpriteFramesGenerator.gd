"""
ASepriteFramesGenerator

Author: nyenye

Brief description: Creates a SpriteFrames resource from Aseprite export files (spritesheet image and json data).

API details:
	1 - Add list of json files to the json_files Array.
	2 - Add list of spritesheet image files to the png_files Array.
	3 - Add list of output SpriteFrames files to the output_files Array.
	4 - Run file from Editor (Ctrl + Shift + X).

Notes:
	- Animation speed (FPS), is calculated from Aseprite frame {duration}.
	- Animation loop, is dervied from Aseprite frameTag {direction}. (true if {direction} == 'pingpong' else false)
	- Errors are added to an Array, and printed at the end of execution.
"""

tool
extends EditorScript

class_name ASepriteFramesGenerator

var json_files = [
	# Player
	"res://Scenes/Entities/King/king.json",
	# Enemies
	"res://Scenes/Entities/Pigs/KingPig/king-pig.json",
	"res://Scenes/Entities/Pigs/Pig/pig.json",
	"res://Scenes/Entities/Pigs/PigBox/pig-box.json",
	"res://Scenes/Entities/Pigs/PigBoom/pig-boom.json",
	"res://Scenes/Entities/Pigs/PigHide/pig-hide.json",
	"res://Scenes/Entities/Pigs/CannonNPig/cannon-n-pig.json",
	# Items
	"res://Scenes/Entities/Items/Bomb/bomb.json",
	"res://Scenes/Entities/Items/Box/box.json",
	"res://Scenes/Entities/Items/Diamonds/diamonds.json",
	"res://Scenes/Entities/Items/Hearts/hearts.json",
	"res://Scenes/Entities/Items/Door/door.json",
	# UI
	"res://Scenes/Entities/LiveBar/hearts.json",
];

var png_files = [
	# Player
	"res://Scenes/Entities/King/king.png",
	# Enemies
	"res://Scenes/Entities/Pigs/KingPig/king-pig.png",
	"res://Scenes/Entities/Pigs/Pig/pig.png",
	"res://Scenes/Entities/Pigs/PigBox/pig-box.png",
	"res://Scenes/Entities/Pigs/PigBoom/pig-boom.png",
	"res://Scenes/Entities/Pigs/PigHide/pig-hide.png",
	"res://Scenes/Entities/Pigs/CannonNPig/cannon-n-pig.png",
	# Items
	"res://Scenes/Entities/Items/Bomb/bomb.png",
	"res://Scenes/Entities/Items/Box/box.png",
	"res://Scenes/Entities/Items/Diamonds/diamonds.png",
	"res://Scenes/Entities/Items/Hearts/hearts.png",
	"res://Scenes/Entities/Items/Door/door.png",
	# UI
	"res://Scenes/Entities/LiveBar/hearts.png",
];

var output_files = [
	# Player
	"res://Scenes/Entities/King/spriteframes-king.tres",
	# Enemies
	"res://Scenes/Entities/Pigs/KingPig/spriteframes-king-pig.tres",
	"res://Scenes/Entities/Pigs/Pig/spriteframes-pig.tres",
	"res://Scenes/Entities/Pigs/PigBox/spriteframes-pig-box.tres",
	"res://Scenes/Entities/Pigs/PigBoom/spriteframes-pig-boom.tres",
	"res://Scenes/Entities/Pigs/PigHide/spriteframes-pig-hide.tres",
	"res://Scenes/Entities/Pigs/CannonNPig/spriteframes-cannon-n-pig.tres",
	# Items
	"res://Scenes/Entities/Items/Bomb/spriteframes-bomb.tres",
	"res://Scenes/Entities/Items/Box/spriteframes-box.tres",
	"res://Scenes/Entities/Items/Diamonds/spriteframes-diamonds.tres",
	"res://Scenes/Entities/Items/Hearts/spriteframes-hearts.tres",
	"res://Scenes/Entities/Items/Door/spriteframes-door.tres",
	# UI
	"res://Scenes/Entities/LiveBar/spriteframes-hearts.tres",
];

func _run() -> void:
	
	if (json_files.size() != png_files.size() || json_files.size() != output_files.size()):
		print("There must be the same number of json, png and output files: (json -> " + str(json_files.size()) + ", png -> " + str(png_files.size()) + ", output -> " + str(output_files.size()) + ")")
		return;
	
	var json_file = File.new();
	var png_file = File.new();
	
	var count = json_files.size();
	
	var errors = [];
	
	for i in count:
		
		var json_file_path : String = json_files[i];
		var png_file_path : String = png_files[i];
		var output_file_path : String = output_files[i];
	
		if (json_file_path.substr(json_file_path.find_last(".")) != ".json"):
			errors.push_back("Must be a JSON file (INDEX = " + str(i) + "): " + json_file_path);
			continue;
	
		if (!json_file.file_exists(json_file_path)):
			errors.push_back("JSON File 404 (INDEX = " + str(i) + "): " + json_file_path);
			continue;
	
		if (png_file_path.substr(png_file_path.find_last(".")) != ".png"):
			errors.push_back("Must be a PNG file (INDEX = " + str(i) + "): " + png_file_path);
			continue;
		
		if (!png_file.file_exists(png_file_path)):
			errors.push_back("PNG File 404 (INDEX = " + str(i) + "): " + png_file_path);
			continue;
		
		json_file.open(json_file_path, File.READ);
		
		var json_string = "";
		
		while not json_file.eof_reached():
			
			json_string += json_file.get_line();
			
			pass;
		
		json_file.close();
		
		if (!validate_json(json_string).empty()):
			errors.push_back("JSON File malformed (INDEX = " + str(i) + "): " + json_file_path);
			continue;
		
		var json_data : Dictionary = parse_json(json_string);
		
		if (!json_data.has("meta") || !json_data.meta.has("frameTags") || !(json_data.meta.frameTags.size() > 0)):
			errors.push_back("JSON Data has no animations (INDEX = " + str(i) + "): " + json_file_path);
			continue;
		
		if (!json_data.has("frames") || !(json_data.frames.size() > 0)):
			errors.push_back("JSON Data has no frames (INDEX = " + str(i) + "): " + json_file_path);
			continue;
		
		var spritesheet : Resource = load(png_file_path);
		
		var animations_data : Array = json_data.meta.frameTags;
		var frames : Array = json_data.frames;
		
		var sprite_frames = SpriteFrames.new();
		sprite_frames.remove_animation("default");
		
		for animation_data in animations_data:
			
			sprite_frames.add_animation(animation_data.name);
			
			var loop = animation_data.direction == "pingpong";
			sprite_frames.set_animation_loop(animation_data.name, loop)
			
			var fps = 1000 / frames[animation_data.from].duration;
			sprite_frames.set_animation_speed(animation_data.name, fps);
			
			for f in range(animation_data.from, animation_data.to + 1):
				
				var frame = frames[f].frame;
				
				var texture = AtlasTexture.new();
				texture.atlas = spritesheet;
				texture.region = Rect2(Vector2(frame.x, frame.y), Vector2(frame.w, frame.h));
				
				sprite_frames.add_frame(animation_data.name, texture);
		
		var save_result = ResourceSaver.save(output_file_path, sprite_frames);
		
		if (save_result != 0):
			errors.push_back("Error (" + str(save_result) + ") saving SpriteFrames resource (INDEX = " + str(i) + ") to : " + output_file_path);
			continue;
	
	print("Errors: ", errors);
	
	return;
