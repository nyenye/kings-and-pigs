extends Node

const GRAVITY = 9.8 * 100;

enum FACING_DIRECTIONS {LEFT = 0, RIGHT = 1};

enum PHYSIC_BITS {
	KING, ENEMY, ZL3, ZL4, ZL5,
	COLLECTIBLE, COLLECTIBLE_AREA, ZL8, ZL9, ZL10,
	ZL11, ZL12, ZL13, ZL14, ZL15,
	WALL_FLOOR, PLATFORM, PLATFORM_ONE_WAY, ZL19, ZL20
};

const ACTIONS : Dictionary = {
	"KING_LEFT": "KING_LEFT",
	"KING_RIGHT": "KING_RIGHT",
	"KING_UP": "KING_UP",
	"KING_DOWN": "KING_DOWN",
	"KING_ATTACK": "KING_ATTACK",
};

const GROUPS: Dictionary = {
	"KING": "KING",
	"COLLECTIBLE": "COLLECTIBLE",
	"HEART":"HEART",
}
