extends KinematicBody2D

var vertical_velocity : float = 0.0;

onready var _Animator: Animator = $Animator;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	vertical_velocity += G.GRAVITY / 3 * delta;

	var linear_velocity = move_and_slide(Vector2(0, vertical_velocity), Vector2.UP);
	
	if (linear_velocity.y == 0):
		vertical_velocity = 0;

func got_collected():
	_Animator.start_animation("Collected");
	_Animator.connect("on_animation_finished", self, "_on_collected_animation_finished");

func _on_collected_animation_finished(anim: String):
	_Animator.disconnect("on_animation_finished", self, "_on_collected_animation_finished");
	queue_free();
