extends Node

onready var _King : King = owner;
onready var _Animator : Animator = _King.get_node("AnimatedSprite");

onready var _Movement : KingMovement = _King.get_node("Behaviours/Movement");
onready var ACTIONS = G.ACTIONS;

const SLIP_COOLDOWN = 0.25;

var slip_timer = 0;

func _process(delta):
	
	if (_King._is_slipping):
		
		slip_timer += delta;
		
		if (slip_timer >= SLIP_COOLDOWN):
			_King._is_slipping = false;

func _input(event : InputEvent):
	
	if (_King._is_dead || _King._is_flinching || _King._is_attacking || _King._is_airborne): return;
	
	if (!_event_is_valid(event)): return;
	
	var action : String = _get_input_action(event);
	
	if (action.empty()): return;
	
	_handle_action(action, event.is_action_pressed(ACTIONS[action]), event.is_action_released(ACTIONS[action]));
	
	return;

func _event_is_valid(event: InputEvent) -> bool:
	
	if (event.device != _King._device): return false;
	
	if (!event.is_action_type()): return false;
	
	if (event.is_echo()): return false;
	
	return true;

func _get_input_action(event: InputEvent) -> String:
	
	if (event.is_action(G.ACTIONS.KING_DOWN)):
		return G.ACTIONS.KING_DOWN;
	
	return "";

func _handle_action(action : String, is_pressed : bool, is_released : bool) -> void:
	
	if (is_released): return;
	
	slip_timer = 0;
	
	_King._is_airborne = true;
	_King._is_slipping = true;
	_King.set_collision_mask_bit(G.PHYSIC_BITS.PLATFORM_ONE_WAY, false);
	
	_Animator.start_animation("Fall");
	return;
