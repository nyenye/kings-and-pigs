extends Node

onready var _King : King = owner;
onready var _Animator : Animator = _King.get_node("AnimatedSprite");

onready var _Movement : KingMovement = _King.get_node("Behaviours/Movement");
onready var ACTIONS = G.ACTIONS;

const JUMP_SPEED = 350;

func _input(event : InputEvent):
	
	if (_King._is_dead || _King._is_flinching || _King._is_attacking || _King._is_airborne): return;
	
	if (!_event_is_valid(event)): return;
	
	var action : String = _get_input_action(event);
	
	if (action.empty()): return;
	
	_handle_action(action, event.is_action_pressed(ACTIONS[action]), event.is_action_released(ACTIONS[action]));
	
	return;

func _event_is_valid(event: InputEvent) -> bool:
	
	if (event.device != _King._device): return false;
	
	if (!event.is_action_type()): return false;
	
	if (event.is_echo()): return false;
	
	return true;

func _get_input_action(event: InputEvent) -> String:
	
	if (event.is_action(G.ACTIONS.KING_ATTACK)):
		return G.ACTIONS.KING_ATTACK;
	
	return "";

func _handle_action(action : String, is_pressed : bool, is_released : bool) -> void:
	
	if (is_released): return;
	
	_King._is_attacking = true
	_Animator.start_animation("Attack");
	_Animator.connect("on_animation_finished", self, "_on_attack_animation_finished")
	
	return;

func _on_attack_animation_finished(animation):
	
	if (animation == "Attack"):
		_King._is_attacking = false;
		_Animator.disconnect("on_animation_finished", self, "_on_attack_animation_finished")
	
	return;
