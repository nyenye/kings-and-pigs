extends Node

onready var _King : King = owner;
onready var _Animator : Animator = _King.get_node("AnimatedSprite");

onready var _Movement : KingMovement = _King.get_node("Behaviours/Movement");

func _process(delta):
	
	_Movement.vertical_speed -= G.GRAVITY * delta;
	
	if (_King._is_slipping): return;
	
	if (_King._is_airborne && _Movement.vertical_speed < 0):
		_Animator.start_animation("Fall")
		_King.set_collision_mask_bit(G.PHYSIC_BITS.PLATFORM_ONE_WAY, true)
	
	return;
