extends Node

onready var _King : King = owner;
onready var _Animator : Animator = _King.get_node("AnimatedSprite");
onready var _AudioStream: KingAudioStream = _King.get_node("AudioStreamPlayer2D")

onready var _Movement : KingMovement = _King.get_node("Behaviours/Movement");
onready var ACTIONS = G.ACTIONS;

var _should_face : int = -1;

func _input(event : InputEvent):
	
	if (_King._is_dead): return;
	
	if (!_event_is_valid(event)): return;
	
	var action : String = _get_input_action(event);
	
	if (action.empty()): return;
	
	_handle_action(action, event.is_action_pressed(ACTIONS[action]), event.is_action_released(ACTIONS[action]));
	
	return;

func _event_is_valid(event: InputEvent) -> bool:
	
	if (event.device != _King._device): return false;
	
	if (!event.is_action_type()): return false;
	
	if (event.is_echo()): return false;
	
	return true;

func _handle_action(action : String, is_pressed : bool, is_released : bool) -> void:
	
	var was_moving = _King._is_moving;
	
	if (is_released):
		
		_Movement.movement_vector = _calc_direction(_Movement.movement_vector, action, is_pressed, false);
		
		_King._is_moving = _Movement.movement_vector.length() > 0;
		
		var new_facing_direction = _get_direction(_Movement.movement_vector);
		
		if (new_facing_direction != -1 && _King._facing_direction != new_facing_direction):
			
			if (_King._is_attacking || _King._is_flinching || _King._is_dead):
				_should_face = new_facing_direction;
				return;
			
			_face_direction(new_facing_direction);
	
	elif (is_pressed):
		
		_King._is_moving = true;
		
		# TODO: Check if opossed action is pressed and pass to _calc_direction (only affects keyboard...)

		_Movement.movement_vector = _calc_direction(_Movement.movement_vector, action, is_pressed, false);
		
		var new_facing_direction = _get_direction(_Movement.movement_vector);
		
		if (new_facing_direction != -1 && _King._facing_direction != new_facing_direction):
			
			if (_King._is_attacking || _King._is_flinching || _King._is_dead):
				_should_face = new_facing_direction;
				return;
			
			_face_direction(new_facing_direction);
	
	if (_King._is_attacking || _King._is_flinching || _King._is_dead || _King._is_airborne):
		return;
	
	if (!was_moving && _King._is_moving && !_King._is_airborne):
		_AudioStream.play_sound(_AudioStream.SOUNDS.STEPS);
		_Animator.start_animation("Run");
	
	return;

func _face_direction(dir: int) -> void:
	
	_should_face = -1;
	
	if dir == _King._facing_direction:
		return;

	_King._facing_direction = dir;

	if (dir == G.FACING_DIRECTIONS.LEFT):
		_Animator.scale.x = -1;
	
	else:
		_Animator.scale.x = 1;
	
	return

func _get_input_action(event: InputEvent) -> String:
	
	if (event.is_action(G.ACTIONS.KING_LEFT)):
		return G.ACTIONS.KING_LEFT;
	
	if (event.is_action(G.ACTIONS.KING_RIGHT)):
		return G.ACTIONS.KING_RIGHT;
	
	return "";

static func _get_direction(movement_direction : Vector2) -> int:
	
	if (movement_direction.x < 0):
		return G.FACING_DIRECTIONS.LEFT;

	if (movement_direction.x > 0):
		return G.FACING_DIRECTIONS.RIGHT;
	
	return -1;

static func _calc_direction(current : Vector2, action : String, is_pressed : bool, is_oposite_pressed : bool) -> Vector2:
	
	if (is_pressed):
		if (action == G.ACTIONS.KING_LEFT):
			current.x = -1;
		elif (action == G.ACTIONS.KING_RIGHT):
			current.x = +1;
	else:
		# TODO: Take into account is_opossite_pressed to correct bug with movement
		
		if (action == G.ACTIONS.KING_LEFT && Input.is_action_pressed(G.ACTIONS.KING_RIGHT)):
			current.x = +1;
		elif (action == G.ACTIONS.KING_RIGHT && Input.is_action_pressed(G.ACTIONS.KING_LEFT)):
			current.x = -1;
		else:
			current.x = 0;
	
	return current;

