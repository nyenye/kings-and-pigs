extends Node

onready var _King : King = owner;
onready var _Animator : Animator = _King.get_node("AnimatedSprite");

func _process(delta):
	
	if (_King._is_moving || _King._is_attacking || _King._is_flinching || _King._is_dead || _King._is_airborne):
		return;
	
	if _Animator.animation == "Ground":
		return;
	
	_Animator.start_animation("Idle");
	
	return;
