extends Node

onready var _King : King = owner;
onready var _Area : Area2D = _King.get_node("Area2D");

func _ready():
	
	_Area.connect("area_entered", self, "_on_area_entered");

func _on_area_entered(area: Area2D):
	
	var otherEntity = area.owner;
	
	if (!otherEntity.is_in_group(G.GROUPS.COLLECTIBLE)): return;
	
	if (otherEntity.is_in_group(G.GROUPS.HEART)):
		otherEntity.got_collected();
		return _King.emit_signal("on_collected_heart");
	
	

