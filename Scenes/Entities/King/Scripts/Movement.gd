extends Node

class_name KingMovement

onready var _King : King = owner;
onready var _Animator : Animator = _King.get_node("AnimatedSprite");
onready var _AudioStream: KingAudioStream = _King.get_node("AudioStreamPlayer2D")

onready var ACTIONS = G.ACTIONS;

const MOVEMENT_SPEED = 80;

var movement_vector : Vector2 = Vector2.ZERO;
var vertical_speed : float = 0.0;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta : float) -> void:
	
	if (!_King._is_moving && !_King._is_airborne):
		vertical_speed = 0;
		return;
	
	if (_King._is_attacking || _King._is_flinching || _King._is_dead):
		vertical_speed = 0;
		return;
	
	if (_King._is_moving && _Animator.animation != "Run" && !_King._is_airborne && _Animator.animation != "Ground"):
		_Animator.start_animation("Run");
	
	var was_falling = _King._is_airborne && vertical_speed < 0;
	
	var linear_velocity = _King.move_and_slide(movement_vector.normalized() * MOVEMENT_SPEED + Vector2(0, -vertical_speed), Vector2(0, -1), true);

	if (linear_velocity.y == 0):
		vertical_speed = 0;
	
	var is_on_floor = _King.is_on_floor();
	
	if was_falling && is_on_floor:
		_Animator.start_animation("Ground")
		_Animator.queue_animation("Idle")
	
	_King._is_airborne = !is_on_floor;
	
	return;
