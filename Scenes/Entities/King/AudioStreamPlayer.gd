extends AudioStreamPlayer2D

class_name KingAudioStream

const SOUNDS : Dictionary = {
	"STEPS": "STEPS",
	"GROUND": "GROUND",
} 

const SFX: Dictionary = {
	SOUNDS.STEPS: preload("res://SFX/footstep02.ogg"),
	SOUNDS.GROUND: preload("res://SFX/footstep03.ogg"),
}

func play_sound(sound):
	stream = SFX[SOUNDS.STEPS];
	play();
