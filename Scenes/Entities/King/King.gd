extends KinematicBody2D

class_name King

# Health
signal on_collected_heart;
signal on_took_hit;

# Currency
signal on_collected_diamod;

export(String) var _subclass_name = "King";

export var _device : int = 0;

var _facing_direction : int = G.FACING_DIRECTIONS.RIGHT;

var _is_moving : bool = false;

var _is_airborne : bool = false;

var _is_slipping : bool = false;

var _is_attacking : bool = false;

var _is_flinching : bool = false;

var _is_dead : bool = false; 

var _targeted_by : int = 0;

func get_class() -> String:
	return "King";

func get_subclass() -> String:
	return _subclass_name;
