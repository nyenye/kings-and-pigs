extends Node

enum BEHAVIOURS {IDLE, PATROL, ATTACK, FLEE, DEATH};

onready var _Behaviour = get_child(BEHAVIOURS.IDLE);

const UPDATE_BEHAVIOUR_COOLDOWN : float =  0.2; # seconds

var _update_behaviour_timer : float = 0.0;
var _current_behaviour = BEHAVIOURS.IDLE;

func _ready():
	
	for behaviour in get_children():
		behaviour.set_process(false);
		behaviour.set_physics_process(false);
	
	_Behaviour.set_process(true);
	_Behaviour.set_physics_process(true);

func _process(delta) -> void:
	_update_behaviour_timer += delta
	
	if (_update_behaviour_timer < UPDATE_BEHAVIOUR_COOLDOWN): return
	
	_update_behaviour_timer = 0
	
	var new_behaviour = _Behaviour.get_updated_behaviour();
	change_behaviour_to(new_behaviour)

func change_behaviour_to(new_behaviour: int) -> void:
	
	if _current_behaviour == new_behaviour: return;
	
	_Behaviour.set_process(false);
	_Behaviour.set_physics_process(false);
	_Behaviour.exited_state();
	
	_current_behaviour = new_behaviour;
	_Behaviour = get_child(new_behaviour);

	_Behaviour.set_process(true);
	_Behaviour.set_physics_process(true);
	_Behaviour.entered_state();

func get_class() -> String:
	return "BehaviourTree";
