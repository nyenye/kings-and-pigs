extends Node

onready var _Pig = owner;

onready var _BehaviourTree = _Pig.get_node("BehaviourTree");
onready var BEHAVIOURS = _BehaviourTree.BEHAVIOURS;

func _ready():
	pass;

func entered_state():
	pass;

func _process(delta):
	pass;

func _physics_process(delta):
	pass;

func exited_state():
	pass;

func get_updated_behaviour():
	
	return BEHAVIOURS.IDLE
