extends Node2D

const MAX_HEALTH : int = 3;

var health = 0;

onready var HeartSprites = [
	get_node("Hearts/1/Animator"),
	get_node("Hearts/2/Animator"),
	get_node("Hearts/3/Animator")
]

onready var _King : King = get_tree().get_nodes_in_group(G.GROUPS.KING)[0];

# Called when the node enters the scene tree for the first time.
func _ready():
	_King.connect("on_collected_heart", self, "add_heart");
	_King.connect("on_took_hit", self, "remove_heart");
	
	for i in range(3):
		var hasHeart = health >= i + 1;
		HeartSprites[i].visible = hasHeart;

func add_heart ():
	
	if (health == 3): return;
	
	health += 1;
	var HeartSprite = HeartSprites[health - 1];
	
	HeartSprite.visible = true;
	HeartSprite.start_animation("Add");
	HeartSprite.queue_animation("Idle");

func remove_heart ():
	
	if (health == 0): return;
	
	health -= 1;
	var HeartSprite = HeartSprites[health];
	
	HeartSprite.start_animation("Remove");
	HeartSprite.connect("on_animation_finished", self, "_on_heart_animation_finished", ['hit', HeartSprite]);


func _on_heart_animation_finished (type, sprite):
	
	if (type == "Remove"):
		sprite.visible = false;
	
	HeartSprites.disconnect("on_animation_finished", self, "_on_heart_animation_finished");
